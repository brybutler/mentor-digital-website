var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;

// Static Server + watching scss/html files
gulp.task('server', function() {

    browserSync({
        proxy: "localhost:8082/",
    });

    gulp.watch(src.html).on('change', reload);
});

// Please **do not** run this task on the build server
gulp.task('default', ['server']);