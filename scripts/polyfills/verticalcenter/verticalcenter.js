$(window).load(function() {

  $('.vertical-center').each(function(index) {
    var $this = $(this);

      var height = $this.outerHeight();
      $this.css('margin-top', '-'+(height/2)+'px');
  });

});