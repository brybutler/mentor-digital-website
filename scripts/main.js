$(document).ready(function() {
    /* NON-BREAKPOINT / NON-BROWSER SPECIFIC INITIATORS ================================================== */
    
    var transition;
    var carouselOptions = {
        nav : true, // Show next and prev buttons
        navText : ['<span class="icon icon-chevron-thin-left"></span>', '<span class="icon icon-chevron-thin-right"></span>'],
        dots : false,
        smartSpeed : 400,
        items : 1,
        loop: true
    };

    // Video poster play
    $(document).on('click tap', '.video-postered .overlay-play', function(event) {
        var $this = $(this);
        var $parent = $this.parent();
        var $iframe = $parent.children('iframe');
        var src = $this.attr('href');
        var autoplay = 'autoplay=true';

        // If it doesn't have a query character add it...
        if(src.indexOf('?') === -1) {
            src += '?';
        }

        // If it does have extra parameters, prepare autoplay...
        if(src.indexOf('&') > -1) {
            src += '&';
        }

        // If it doesn't have autoplay, add it...
        if(src.indexOf(autoplay) === -1) {
            src += autoplay; // Could have an issue if some variables are set already...
        }

        // Set src...
        $iframe.attr('src', src);

        // Swap out items...
        $this.hide();
        $iframe.show();

        // Fitvid
        $parent.fitVids();

        event.preventDefault();
        return false;
    });

    // Collapsibles... Bootstrap didn't work with dnyamic content
    $('.collapse').css('max-height', 0);

    $(document).on('click tap', '[data-toggle="collapse"]', function (event) {
        var $this   = $(this);
        var target = $this.attr('href');
        var $target = $(target);
        var transition = transitionEnd();
        var scrollHeight = $target[0].scrollHeight;
        
        $this.toggleClass('collapsed');
        $target.addClass('collapsing');

        if($this.hasClass('collapsed')) { // Collapse it...
            $target.css('max-height', scrollHeight);
            setTimeout(function() {$target.css('max-height', 0);}, 1);
        } else {
            $target.css('max-height', 0);
            setTimeout(function() {$target.css('max-height', scrollHeight);}, 1);
        }

        if(transition) {
            $target.on(transition, function(){ // After it has transitioned away...
                $target.removeClass('collapsing');
                if($this.hasClass('collapsed')) {
                } else {
                    $target.css('max-height', '');
                }
            });
        } else {
            $target.removeClass('collapsing');
            if($this.hasClass('collapsed')) {
            } else {
                $target.css('max-height', '');
            }
        }

        event.preventDefault();
    });

    function transitionEnd() {
      var el = document.createElement('div')//what the hack is bootstrap

      var transEndEventNames = {
        WebkitTransition : 'webkitTransitionEnd',
        MozTransition    : 'transitionend',
        OTransition      : 'oTransitionEnd otransitionend',
        transition       : 'transitionend'
      }

      for (var name in transEndEventNames) {
        if (el.style[name] !== undefined) {
          return transEndEventNames[name];
        }
      }

      return false // explicit for ie8 (  ._.)
    }

    // Modal
    $('body').on('click tap', '[data-toggle="modal"]', function(event) {
        var $this = $(this);
        var $target = $($this.attr('href'));

        if($target.length > 0) {
            $target.addClass('active');
        }

        event.preventDefault();
        return false;
    });

    $('body').on('click tap', '.modal, .modal-inner, .modal-background', function(event) {
        if(event.target == this) {
            $('.modal').removeClass('active');
        }
    });


    // handheld main navigation 
    $(function () {
        var pull = $('.btn-menu');
        var close = $('.btn-close');
        var body = $('body,html');
        menu = $('.menu');
        menuHeight = menu.height();

        $(pull).on('click tap', function (e) {
            menu.stop().fadeToggle();
            body.addClass('menu-down');
            pull.addClass('menu-down');
            close.addClass('menu-down');
            $('.search-bar').fadeToggle();
            e.stopPropagation();
            e.preventDefault();
            return false;
        });

        $(window).resize(function () {
            var w = $(window).width();
            if (w > 200 && menu.is(':hidden')) {
                menu.removeAttr('style');
            }
        });
    });

    $('.btn-close').on("click tap", function (event) {
        $('.menu').fadeOut();
        $(this).removeClass("menu-down");
        $('.btn-menu').removeClass("menu-down");
        $('body,html').removeClass("menu-down");
        event.preventDefault();
        return false;
    });

    //OWL CAROUSEL SINGLE CAROUSEL
    if($('.owl-single-carousel').length > 0) { // There are many, so need to do an each on this...
        var $carousels = $('.owl-single-carousel');

        $carousels.each(function(index) {
            var $this = $(this);

            var single = $this.owlCarousel(carouselOptions);
            single.owlCarousel();

            single.on('translate.owl.carousel', function(event) {
                $(this).addClass('transitioning');
            });

            single.on('translated.owl.carousel dragged.owl.carousel', function(event) {
                $(this).removeClass('transitioning');
            });

            if(single.hasClass('carousel-casestudies')) { // Doesn't work below ie9, so bugger it
                if(!$('html').hasClass('lt-ie9') && !$('html').hasClass('ie9')) {
                    var $stage = single.find('.owl-stage');
                    var nudgeAmount = 100; //px

                    // Watch for history change so that we can update the carousel position...
                    History.Adapter.bind(window, 'statechange', function() { // Being fired twice...
                        // Then, we need to figure out why the current index is same as the previous index when using back button...
                        // It's like it's 1 step behind... 3, 4, 5...
                        // Go back and it's... 5, 4, 3... it gets the right URL but it has the wrong passed index??? So weird...
                        //if (window.ajaxlinkInternalChange) { return; } // Basically, if we set the history, then ignore it, because carousel is fine...
                        var state = History.getState(); 
                        var settings = state.data.settings;
                        var passedIndex = settings.historypass;
                        var total = single.data('owlCarousel')['_items'].length;
                        var currentIndex = single.data('owlCarousel')['_current']; // This is actually behind...
                        
                        // If the passed index is not the same as the current index, let's change the carousel position!
                        if(currentIndex !== passedIndex) {
                            if(passedIndex > total) {
                                passedIndex = passedIndex - total;
                            }

                            single.off('changed.owl.carousel');
                            single.trigger('to.owl', [passedIndex+1, 100, true]);// IE 8 and 9 bug... basically, it reverts, and I think it happens here...

                            // Need to wait for the change to be over before we watch for changed...
                            single.on('changed.owl.carousel', function(event) {
                                single.off('changed.owl.carousel');
                                single.on('changed.owl.carousel', function(event) {
                                    changeCasestudy(event);
                                });
                            });
                        }
                    });

                    // We want translation there for our hover effects...
                    $stage.css('transition', '0.4s');
                    $stage.css('-webkit-transition', '0.4s');
                    single.on('translated.owl.carousel dragged.owl.carousel resized.owl.carousel', function(event) {
                        $stage.css('transition', '0.4s');
                        $stage.css('-webkit-transition', '0.4s');
                    });

                    single.on('changed.owl.carousel', function(event) {
                        changeCasestudy(event);
                    });
                    
                    single.find('.owl-prev').on('mouseenter', function(event) {
                        $stage.css('margin-left', nudgeAmount+'px');
                    });

                    single.find('.owl-next').on('mouseenter', function(event) {
                        $stage.css('margin-left', '-'+nudgeAmount+'px');
                    });

                    single.find('.owl-prev,.owl-next').on('mouseleave', function(event) {
                        $stage.css('margin-left', '');
                    });
                } else {
                    $('.carousel-casestudies .ajaxlink').removeClass('ajaxlink');
                }
            }
        });
    }

    function changeCasestudy(event) {
        var $carousel = $(event.target);
        var current = event.item.index;
        var $current = $carousel.find('.owl-item').eq(current);
        var $link = $current.find('.ajaxlink');

        // Set data for our history thing to pick up...
        $link.data('historypass', current);

        // Click to fire ajaxlink...
        $link.click();
    }

    // Ajax links
    if($('.ajaxlink').length > 0) { // If there is no ajaxlink, but they go back into a page that was from an ajaxlink, it breaks... Hrum...
        var options = {
            onComplete: function() {
                initPlugins();
                if($('#ajaxcontent .owl-single-carousel').length > 0) {
                    $('#ajaxcontent .owl-single-carousel').owlCarousel(carouselOptions);
                }
            }
        };

        if($('.owl-single-carousel .carousel-casestudies').length > 0) {
            options.historypass = $('.owl-single-carousel .carousel-casestudies .owl-item.active').index();
        }

        $('.ajaxlink').ajaxLink(options);
    }



    function initPlugins() {
        // Text restrictions
        if($('.text-limit').length > 0 && $.fn.dotdotdot) {
            $('.text-limit').dotdotdot({});
            setTimeout(function(){$('.text-limit').dotdotdot({});}, 1); // Twice, because the dots can cause it to break to a new line... lol
        }
        // Vimeo video size
        if($('.fitvid').length > 0) {
            $('.fitvid').fitVids();
        }
        // Comparitor slider thing
        if($('.compare').length > 0) {
            $('.compare').twentytwenty();
        }
    }
    initPlugins();

    /* =================================================================================================== */

    /* BREAKPOINT / BROWSER SPECIFIC INITIATORS ========================================================== */
    //Breakpoint configuration
    var jRes = jRespond([{
        label: 'handheld',
        enter: 0,
        exit: 767
        //exit: 480
    }, {
        label: 'desktop',
        enter: 768,
        //enter: 481,
        exit: 10000
    }]);

    //If IE8 or lower initiate desktop js, otherwise add breakpoint event handlers.
    if ($("html").hasClass("lt-ie9")) {
        enterDesktopJS();
    } else {
        /* HANDHELD BREAKPOINT DETECTOR ================================================================== */
        jRes.addFunc({
            breakpoint: 'handheld',
            enter: function() {
                enterHandheldJS();
            }
        });
        /* =============================================================================================== */

        /* DESKTOP DETECTOR ============================================================================== */
        jRes.addFunc({
            breakpoint: 'desktop',
            enter: function() {
                enterDesktopJS();
            }
        });
        /* =============================================================================================== */
    }

    /* HANDHELD INITIATORS =============================================================================== */
    function enterHandheldJS() {
        //window.console && console.log("handheld breakpoint active");


        
        // sub-navigation
        $(function() {
            var pulli = $('#pull-i');
            menui = $('ul.sub-navigation');
            menuHeight = menui.height();

            $(pulli).on('click', function(e) {
                e.preventDefault();
                menui.slideToggle();
                pulli.toggleClass("menu-down");
            });
        });

        // Swipe notification
        $(function() {
            var $tableContainer = $('.table-responsive');
            $tableContainer.addClass('notify');

            // On scroll/tap/click then we hide the notification... quite simple really
            $tableContainer.on('scroll tap click', function(index, elem) {
                $tableContainer.removeClass('notify');
                $tableContainer.off();
            });
        });

        // switch owl slider desktop image to mobile
        $.each($("#hero #owl-single-carousel .owl-item img"), function (e) {
            var mobileImage = $(this).attr("data-mob-src");
            $(this).attr("src", mobileImage);
        });
    }
    /* =================================================================================================== */

    /* DESKTOP INITIATORS ================================================================================ */
    function enterDesktopJS() {
        //window.console && console.log("desktop breakpoint active");
        // Turn off mobile main nav button
        $(".btn-menu").off();

        /* Disable trigger and remove style from submenu */
        if ($(".sub-navigation-block").length) {
            var pulli = $('#pull-i');
            menui = $('ul.sub-navigation');
            $(pulli).off();
            if (menui.is(':hidden')) {
                menui.removeAttr('style');
            }
        }

        // switch owl slider mobile image to desktop
        $.each($("#hero #owl-single-carousel .owl-item img"), function (e) {
            var desktopImage = $(this).attr("data-dt-src");
            $(this).attr("src", desktopImage);
        });
    }
    /* ================================================================================================== */
});

/* HELPERS ============================================================================================== */

/* ====================================================================================================== */