/*global jQuery */
/*!
* Re-usable way to load page content asynchronously... container should be the same as the link's container, otherwise a refresh is going to be weird
*/
(function( $ ){
  $.fn.ajaxLink = function(options) {
    // Passed settings
    var defaults = $.extend({
      historypass: 44,
      target: '#ajaxcontent',
      scroll: false, // Want to animate scroll to top of ajax container?
      append: false, // What to append the content to the ajax container or replace the content?
      slide: true, // Do we want to animate the height of the container?
      paginated: false, // Hmm... can it work with a data attribute... it increments the last number in the URL... a bit hacky, but meh...
      expected: 9,
      cached: true,
      // Should probably add an option to preserve... so that when going back, the content is just set instead of loaded again...
      onComplete: function() { return undefined; }
    }, options || {});

    // Other variables...
    var selector = this.selector;
    var $throbber = $('<div class="ajaxlink-overlay"><div class="ajaxlink-throbber"><div class="spinner"></div></div></div>');
    var initialized = false;
    var cachedIndex = 0;
    var previousSettings;

    // Get us a new id for cached content...
    var generateCachedId = function() {
      cachedIndex++;

      return 'al-cached-'+(cachedIndex);
    }

    // Cache content...
    var cacheContent = function(html, id) {
      if(id === undefined) {
        id = generateCachedId();
      }

      $('<div/>', {
        'class': 'al-cached',
        id: id,
        style: 'display: none',
        html: html
      }).appendTo('body');
    }

    // Finds the last number in the string and increments it...
    var incrementLast = function(v) {
      return v.replace(/[0-9]+(?!.*[0-9])/, function(match) {
          return parseInt(match, 10)+1;
      });
    }

    // Go through data variables... what we're doing is making sure the element data variables override the default variables...
    var dataOverrides = function($link) {
      var temp = $.extend({}, defaults); // Get the current settings...
      temp.url = $link.attr('href'); // get the link we're going to ajax request to
      temp.id = $link.data('unique-id');
      temp.target = $link.data('target'); // the unique identifier for the content we want... should be the same in both places probably
      temp.append = $link.data('append');
      temp.scroll = $link.data('scroll');
      temp.slide = $link.data('slide');
      temp.paginated = $link.data('paginated');
      temp.expected = $link.data('expected');
      temp.cached = $link.data('cached');
      temp.cachedId = $link.data('cached-id');
      temp.back = $link.data('back');
      temp.historypass = $link.data('historypass');

      // If the data variable isn't set, then make sure we use the settings value... probably a smarter way to do this
      if(temp.target === undefined) {
        temp.target = defaults.target;
      }
      if(temp.append === undefined) {
        temp.append = defaults.append;
      }
      if(temp.scroll === undefined) {
        temp.scroll = defaults.scroll;
      }
      if(temp.slide === undefined) {
        temp.slide = defaults.slide;
      }
      if(temp.paginated === undefined) {
        temp.paginated = defaults.paginated;
      }
      if(temp.expected === undefined) {
        temp.expected = defaults.expected;
      }
      if(temp.cached === undefined) {
        temp.cached = defaults.cached;
      }

      // Setting a unique ID on the link because History doesn't like objects being passed into it and we may need to change the ajaxlink source (hiding it) later
      if(temp.id === undefined) {
        temp.id = new Date().getTime();
      }

      if(temp.cached) {
        // This ID is used to set preserve container ID, detached, and then look it up later...
        if (temp.cachedId === undefined) {
          temp.cachedId = generateCachedId(); // Just add 1 to temp id, doesn't really matter and we want to make sure it's not the same time?
        }
        if (temp.cachedScroll === undefined) {
          temp.cachedScroll = $(document).scrollTop(); // Just add 1 to temp id, doesn't really matter and we want to make sure it's not the same time?
        }
      }

      // Making function stay for History PushState... meh, scrapped this idea... was going to pass it through as a name, but realised I probably can't, so just used defaults.onComplete instead...
      //temp.onComplete = 

      return temp;
    }

    function transitionEnd() {
      var el = document.createElement('div')//what the hack is bootstrap

      var transEndEventNames = {
        WebkitTransition : 'webkitTransitionEnd',
        MozTransition    : 'transitionend',
        OTransition      : 'oTransitionEnd otransitionend',
        transition       : 'transitionend'
      }

      for (var name in transEndEventNames) {
        if (el.style[name] !== undefined) {
          return transEndEventNames[name];
        }
      }

      return false // explicit for ie8 (  ._.)
    }

    // Functions
    var replaceContent = function($container, content) {
      var prevHeight = $container.outerHeight();
      var transition = transitionEnd();
      // I think it's the transition... jquery knows there is a transition, but IE is like "Meh, fuck transitions"...

      // Transition out current content

      // Check if the container has a transition, if so we should wait for it, otherwise meh...
      if(transition) {
        $container.addClass('replacing');
        $container.css('max-height', prevHeight); // Should try to wrap this stuff and do a check on slide

        $container.on(transition, function() { // After it has transitioned away...
          $container.off();

          // Replace content after hidden...
          $container.html(content);

          // Now let's reveal again...
          $container.removeClass('replacing');

          // Changing scroll height, and need to set to auto after it has transitioned to make sure it functions well for screen resizing etc
          $container.css('max-height', $container[0].scrollHeight);
          $container.on(transition, function(){
            $container.off();
            $container.css('max-height', '');
            $('.ajaxlink-overlay').hide();
            //console.log(defaults.onComplete);
            defaults.onComplete();
          });
        });
      } else {
        $container.html(content);
        defaults.onComplete();
      }
    };

    var appendContent = function($container, content) {
      var prevHeight = $container.outerHeight();
      var transition = transitionEnd();

      // Check if the container has a transition, if so we should wait for it, otherwise meh...
      if(transition) {//$container.is(':animated')) {
        // Transition out current content
        $container.addClass('appending');
        $container.css('max-height', prevHeight);

        // Append content after hidden...
        $container.append(content);

        // Sliding max-height...
        $container.css('max-height', $container[0].scrollHeight);
        $container.on(transition, function(){
          $container.off();
          $container.css('max-height', '');
          $('.ajaxlink-overlay').hide();
          //console.log(defaults.onComplete);
          defaults.onComplete();
        });

        $container.removeClass('appending');
      } else {
        $container.append(content);
        defaults.onComplete();
      }
    };

    var setContent = function(settings, $content) {
      var $link = $('[data-unique-id="'+settings.id+'"]');
      var $container = $(settings.target);
      var content = $content.html();

      // There is a bug which I can't quite figure out... basically if there is more than 1 preserved div, the content starts getting a bit mixed up...
      //$('.ajaxlink-preserved').remove();

      // Add in the content...
      if(settings.append) { // If appending...
        appendContent($container, content);
      } else {
        replaceContent($container, content);
      }

      // Scroll to top of container...
      if(settings.scroll) {
        $('html, body').animate({
            scrollTop: $container.offset().top
        }, 250);
      }

      // Remove button if we get less than the expected amount...
      if(settings.append && $content.children().length < settings.expected) {
        $link.detach();
      }

      // Hide throbber...
      $('.ajaxlink-overlay').hide();
    }

    var getContent = function(settings) { // Get content for the link...
      var $link = $('[data-unique-id="'+settings.id+'"]');
      var $container = $(settings.target);

      // Show throbber because we're about to make an AJAX call
      if($('.ajaxlink-overlay').length === 0) {
        $container.prepend($throbber);
      }
      $('.ajaxlink-overlay').show();

      // Preserve content... going back causes us to save the current content when link is preserve... is this what we want?
      if(settings.cached && $('#'+settings.cachedId).length > 0) {
        var $content = $('#'+settings.cachedId);
        setContent(settings, $content);

        $content.remove();
      } else {// Make the request for the page
        $.ajax({
          method: 'GET',
          url: settings.url,
          //dataType: 'html',
          success: function (data) {
            // Set html data
            var $html = $(data);
            var $content = $html.find(settings.target);
            
            // Do a check to see if target exists, if not, just go to the page because this isn't going to work...
            if($content.length > 0 && $container.length > 0) {
              // Set the content...
              setContent(settings, $content);
            } else {
              //console.log('Content or container not found');
              document.location.href = settings.url;
            }
          },
          error: function () { // If it fails, just go to the page, because the page can tell the user what's up...
            //console.log('AJAX error');
            document.location.href = settings.url;
          }
        });
      }
    }

    // Watch for history change...
    History.Adapter.bind(window, 'statechange', function() {
      var state = History.getState();
      var settings = state.data.settings;

      // Init
      if(initialized) { // Ignoring replace...
        // Current state
        var $container = $(settings.target);
        settings.url = state.hash; // Need to make sure we're going with the URL that is stored, not the URL of the link that was passed... this is because of the first request that is stored...
        
        if(settings.cached) { // For some reason, sometimes the previousSettings isn't set...
          cacheContent($container.html(), previousSettings.cachedId);
        }

        getContent(settings);
      }

      // Save settings... we do this because we need the previous page's cache ID...
      previousSettings = settings;
    });


    // Making the click event "live" because we content changes, and we need to make sure we watch for ajax links inside content too...
    $(document.body).on('click tap', selector, function(event) {
      // Init
      var $link = $(this);
      var settings = dataOverrides($link);

      // So, we are caching all by default, which is good...
      // However, the initial one is the problem... basically, the cachedId is not used... so we click a link, and it generates the cachedId... we need to update the initial state with this cachedId really...

      if(settings.back) {
        var current = History.getCurrentIndex();
        var previous = History.getStateByIndex(current-2);

        // If it's there (we tracked it), go back...
        if(previous !== undefined) {
          History.back();
          event.stopPropagation();
          event.preventDefault();
          return false;
        }
      }

      // Save cached ID for the link... idea being we load up a page with preserve on, we go back, and if we click the same link we should load up the saved content...
      if(settings.cached) {
        $link.attr('data-cached-id', settings.cachedId);
      }

      // If we're appending, then we're going to stay on the current page without touching the URL
      if(settings.append) { 
        getContent(settings);
      } else {// The reason why we have to fire requests from the URL change, is because if we don't, then we aren't handling the back requests via AJAX...
        window.ajaxlinkInternalChange = true;
        History.pushState({settings: settings}, null, settings.url);
        window.ajaxlinkInternalChange = false;
      }

      // Increment pagination if it's there... we are just changing the last number we see, a bit hacky but meh it'll do for now...
      if(settings.paginated) {
        $link.attr('href', incrementLast(settings.url));
      }

      event.stopPropagation();
      event.preventDefault();
      return false;
    });


    function init() { // I could do al-cached-x, and just increment this? First one is 1, so basically if not defined go with 1?
      var settings = defaults;
      settings.cachedId = generateCachedId();
      previousSettings = settings;

      History.replaceState({settings: settings}, null, document.location.href);
      initialized = true;
    }

    init();

    // Return the jQuery object to allow for chainability.
    return this;
  };
})( jQuery );